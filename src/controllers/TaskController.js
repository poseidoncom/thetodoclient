import setup from './setup.json';


export async function create(task){
    const response = await fetch(setup.url + "task", {
        method: 'POST', // *GET, POST, PUT, DELETE, etc.
        headers: {
          'Content-Type': 'application/json'  
        },
        body: JSON.stringify(task) // body data type must match "Content-Type" header
      });
      return response.json(); // parses JSON response into native JavaScript objects
}

export async function update(task){
  const response = await fetch(setup.url + "task", {
      method: 'PUT', // *GET, POST, PUT, DELETE, etc.
      headers: {
        'Content-Type': 'application/json'  
      },
      body: JSON.stringify(task) // body data type must match "Content-Type" header
    });
    return response.json(); // parses JSON response into native JavaScript objects
}

export async function getTasks(){
    return fetch(setup.url+"tasks")
    .then(response => response.json())
    .then(data => {return(data)});
}

export async function getCategories(){
    return fetch(setup.url+'categories')
    .then(response => response.json())
    .then(data => {return(data)});
}

export async function createCategory(category){
  const response = await fetch(setup.url + "category", {
      method: 'POST', // *GET, POST, PUT, DELETE, etc.
      headers: {
        'Content-Type': 'application/json'  
      },
      body: JSON.stringify(category) // body data type must match "Content-Type" header
    });
    return response.json(); // parses JSON response into native JavaScript objects
}

