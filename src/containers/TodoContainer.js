import React, { useState, useRef, useEffect} from "react";
import { create, update, getCategories, getTasks } from "../controllers/TaskController";
import TaskList from "../components/TaskList";
import TaskComponentAdd from "../components/TaskComponentAdd";
import TaskComponentEdit from "../components/TaskComponentEdit";
import NavComponent from "../components/NavComponent";

const TodoContainer = () => {

    const [task, setTask] = useState([]);
    const [tasks, setTasks] = useState([]);
    const [categories, setCategories] = useState([]);
    const [isEditing, setIsEditing] = useState(false);
    //nykyinen taski tarvitaan taskin muokkausta varten
    const [currentTask, setCurrentTask] = useState({});

    //hakee RESTapista tehtävät ja kategoriat
    useEffect(() => {
      initData();
    }, [])

    async function initData(){
        getCategories()
        .then(data=>{
            setCategories(data)
            console.log(categories);
        })
        .catch((error) => {
            alert('Error:', error);
        });

        getTasks() 
        .then(data=>{
            setTasks(data); 
        })
        .catch((error) => {
            alert('Error:', error);
        });

    }

    useEffect(()=>{
        localStorage.setItem("task", JSON.stringify(task));
    },[task])
    

    const handleChange = (e) => {
        let taskNew = {...task}; 
        taskNew[e.target.name]=e.target.value;
        setTask(taskNew); 
    };

    //käsittelee tehtävän päivitykset
    const handleChange2 = (e) => {
        //console.log(e.target.type);
        let taskUpdate = {...currentTask};
        if(e.target.type == "checkbox" ){
            taskUpdate[e.target.name]=e.target.checked;
        }
        else{
            taskUpdate[e.target.name]=e.target.value;
        }
        
        console.log(JSON.stringify(currentTask));
        setCurrentTask(taskUpdate); 
    };

    const handleSubmit = async() =>{
        const result = await create(task);
        //alert("Task added to database"+ JSON.stringify(task));
        initData();
    }
    const handleUpdate = async() =>{   
    
        const result = await update(currentTask);
        //alert("Task updated"+ JSON.stringify(currentTask));
        initData();
        setIsEditing(false);
        
    }


    //käsittelee edit-napin klikkauksen
    const handleEditClick = (task) => {
        
        console.log("editin taski",task)
        // asettaa muokkaustilan päälle
        setIsEditing(true);
        // tallettaa valitun taskin tiedot currentTaskiin
        setCurrentTask({ ...task });
        // scrollaa sivun yläreunaan
        executeScroll()
      }

   const myRef = useRef(null);

   const executeScroll = () => myRef.current.scrollIntoView();
    
    



    return(
        <div ref={myRef} className="todoapp">
            <h1>TheToDoApp</h1>
            <NavComponent/>

            {isEditing ? (
                
                <div >
                    <h2>Edit task</h2>
                    <TaskComponentEdit class={"editClass"} task={currentTask} submit={handleUpdate} categories={categories} change={handleChange2}/>
                    <button className="button-1" onClick={() => setIsEditing(false)}>Cancel</button>
                    
                </div>
    
            ):(
                <div>
                    <h2>Add task</h2>
                    <TaskComponentAdd  class={"addClass"} task={task} submit={handleSubmit} categories={categories} change={handleChange} />
                </div>
            )}

            <h2>Tasks:</h2>

            <TaskList class={"listClass"} edit={handleEditClick} data={tasks} categories={categories} />

        </div>
    )
}

export default TodoContainer;