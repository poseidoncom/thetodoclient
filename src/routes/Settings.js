import React, {useState} from 'react';
import NavComponent from '../components/NavComponent';
import { createCategory } from '../controllers/TaskController';

const Settings = () => {
    const [category, setCategory] = useState([]);


    const handleSubmit = () =>{
        const result = createCategory(category);
        //alert("Category added to database"+ JSON.stringify(category));
    }

    const handleChange = (e) =>{
        let newCategory = {...category};
        newCategory[e.target.name]=e.target.value;
        setCategory(newCategory);
        
    }

  return (

    <div>
        <NavComponent/>
        <div>
            <h2>Settings</h2>
            <h3>Add category: </h3>
            <input onChange={e=>handleChange(e)} name="name" />
            <button onClick={handleSubmit}>Add</button>
            
        </div>
    </div>
  )
}

export default Settings