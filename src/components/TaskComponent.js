import React, {useEffect,useState} from 'react'



const TaskComponent = (props) => {

    const [showMore, setShowMore] = useState(true);
    const [toteuma, setToteuma] = useState([]);
    const [seconds, setSeconds] = useState(0);
    const [isActive, setIsActive] = useState (false);

    const categoryName = props.categories.map((category)=>{
      if (category.id === props.task.categoryId){
        return <h4>{category.name}</h4>
      }
    }
    )
    useEffect(()=>{
      handleToteuma();
    })
    
    // I used and modified this example for timer https://upmostly.com/tutorials/build-a-react-timer-component-using-hooks
    useEffect(() => {
      let interval = null;
      if (isActive) {
        interval = setInterval(() => {
          setSeconds(seconds => seconds+1);
        }, 1000);
        props.task.usedTime=props.task.usedTime + (1/3600);
      } else if (!isActive && seconds !== 0) {
        clearInterval(interval);
      }
      
      return () => clearInterval(interval);
      
    }, [isActive, seconds]);



    const handleToteuma = () =>{
      if(props.task.usedTime===0){
        setToteuma("Ei merkintöjä");
      }
      else{
        setToteuma(props.task.usedTime.toFixed(3));
      }
    }

  

    function toggle(){
      setIsActive(!isActive);
      setSeconds(0);
    }
    

    




  return (
    
    <div className="listClass">
      
            
      {console.log("taskComponent ",props.task.id,  " rerendered")}
                <div className='tasks'>
                    
                    <tr><td><h2 style={props.task.done ? ({textDecoration: "line-through",color: "grey"}):({textDecoration: "none"})}>{props.task.description}</h2></td></tr>
                    {showMore ? (
                        <button class="button-1" onClick={() => setShowMore(false)}>more</button>
                    ):(
                
                        <table><tr><td>Projekti: </td><td>{categoryName}</td></tr><tr><td>Osatavoitteet:</td>
                        <td><ul>
                          <li>{props.task.subObjective1}</li>
                          <li>{props.task.subObjective2}</li>
                        </ul></td>
                        </tr>
                        <tr><td>Suunniteltu</td><td>{props.task.plannedTime}</td><td>tuntia</td></tr>
                        <tr><td>Toteuma</td><td>{toteuma}</td><td>tuntia</td></tr>
                        <tr><td><h2>Ajastin:</h2></td><td><h3>{seconds}s</h3></td><td><button className='button-1' onClick={toggle}>{isActive ? 'Pysäytä' : 'Käynnistä'}</button></td></tr>
                        <tr><td><button className="button-1" onClick={() => props.handleEdit(props.task)}>Edit/Save</button></td><td><button className='button-1' onClick={()=> setShowMore(true)}>less</button></td></tr>
                        </table>
                      
                    )}
                    
                    
                    

                </div>

            
    </div>
  )
}

export default TaskComponent