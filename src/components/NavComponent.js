import React from 'react'
import {Link} from "react-router-dom";

const NavComponent = (props) => {
  return (
    <nav
        style={{
          borderBottom: "solid 1px",
          paddingBottom: "1rem",
        }}
      >
        <Link to="/">Home</Link> |{" "}
        <Link to={{
          pathname: "/settings",
          state: props.task,
        }}
          >Settings</Link> 
        
      </nav>
  )
}

export default NavComponent