import React from 'react'


const TaskComponent = (props) => {


    const categoryOptions = props.categories.map((category, index)=>{
        return <option key={parseInt(index)} value={category.id}>{category.name}</option>
    }
    )


  return (
    <div>

            <div className={props.class}>

                <table> 
                    <tr><td>Kuvaus:</td><td><input onChange={e=>props.change(e)} placeholder="Palauta tehtävä" name="description" type="text" defaultValue={props.task.description}></input></td>
                    
                    </tr>
                    <tr><td>Projekti:</td><td>
                        <select onChange={e=>props.change(e)} defaultValue={props.task.categoryId} name="categoryId">
                            {categoryOptions} 
                        </select>
                    </td></tr>
                    <tr><td>Osatavoitteet:</td>
                    <td><ul style={{listStyleType:'none'}}>
                        <li><input onChange={e=>props.change(e)} placeholder="1. osatavoite" type="text" name="subObjective1" defaultValue={props.task.subObjective1}></input></li>
                        <li><input onChange={e=>props.change(e)} placeholder="2. osatavoite" type="text" name="subObjective2" defaultValue={props.task.subObjective2}></input></li>
                        </ul></td></tr>
                    <tr><td>Suunniteltu kesto</td><td><input onChange={e=>props.change(e)} placeholder="2" type="number" name="plannedTime" defaultValue={props.task.plannedTime}></input></td><td>tuntia</td></tr>
                    <tr><td>Toteutunut kesto</td><td><input onChange={e=>props.change(e)} placeholder="3" type="number" name="usedTime" defaultValue={props.task.usedTime}></input></td><td>tuntia</td></tr>
                    <tr><td>Suoritettu:</td><td><input type="checkbox" name='done' defaultChecked={props.task.done} onChange={e=>props.change(e)}></input></td></tr>
                    
                    <tr><td><button class="button-1" onClick={props.submit}>Save task</button></td></tr>

                </table>
            </div>
    </div>
  )
}

export default TaskComponent