import React from 'react'


const TaskComponent = (props) => {


    const categoryOptions = props.categories.map((category, index)=>{
        return <option key={parseInt(index)} value={category.id}>{category.name}</option>
    }
    )


  return (
    <div>

            <div className={props.class}>

                <table> 
                    <tr><td>Kuvaus:</td><td><input onChange={e=>props.change(e)} placeholder="Palauta tehtävä" name="description" type="text" defaultValue={props.task.description}></input></td>
                    
                    </tr>
                    <tr><td>Projekti:</td><td>
                        <select onChange={e=>props.change(e)} name="categoryId">
                            <option value="" selected disabled hidden>Valitse</option>
                            {categoryOptions} 
                        </select>
                    </td></tr>
                    <tr><td>Osatavoitteet:</td>
                    <td><ul style={{listStyleType:'none'}}>
                        <li><input onChange={e=>props.change(e)} placeholder="1. osatavoite" type="text" name="subObjective1" defaultValue={props.task.subObjective1}></input></li>
                        <li><input onChange={e=>props.change(e)} placeholder="2. osatavoite" type="text" name="subObjective2" defaultValue={props.task.subObjective2}></input></li>
                        </ul></td></tr>
                    <tr><td>Suunniteltu aika</td><td><input onChange={e=>props.change(e)} placeholder="2" type="number" name="plannedTime" defaultValue={props.task.plannedTime}></input></td><td>tuntia</td></tr>
                    
                    <tr><td><button class="button-1" onClick={props.submit}>Save task</button></td></tr>

                </table>
            </div>
    </div>
  )
}

export default TaskComponent