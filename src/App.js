import logo from './logo.svg';
import './App.css';
import TodoContainer from './containers/TodoContainer';


function App() {
  return (
    <div className='app'>
      <TodoContainer/>
      
    </div>
  );
}

export default App;
